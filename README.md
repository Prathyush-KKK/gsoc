# Personal Information
Name: Prathyush Kodhanpur <br>
Email: prathyushkodhanpur@gmail.com <br>
Github: [Prathyush-KKK](https://github.com/Prathyush-KKK) <br>
Location: Bangalore, India <br>
Time Zone: UTC/GMT +5:30 hours <br>
University: Vellore Institute of Technology, Vellore <br>
Major: Bachelors in Computer Science and Engineering  <br>

# About Me
I am a second-year undergraduate student pursuing a Bachelor's degree in Computer Science and Engineering at Vellore Institute of Technology, Vellore, India. I began my programming journey with Python, and subsequently learned object-oriented programming and data structures using C/C++, achieving commendable grades in both subjects.

As I ventured into web development, I delved into JavaScript and became proficient in the REACT framework. I am currently expanding my skill set by learning Next.js on the front-end side. Furthermore, my interest in back-end development led me to become familiar with Node.js.

In addition to my technical abilities, I possess design skills and have experience in UI/UX design, utilizing software such as Figma and Adobe Illustrator. I am also proficient in Adobe Photoshop and Adobe Premiere Pro.

Throughout my college years, I have actively participated in numerous clubs and chapters such as IEEE Computer Science Society and CodeChef, contributing to the coding, web/app designs, and creative processes such as posters and videos.

I have collaborated on various college-centric projects with my organizations, including the development of a laundry tracker app, mess menu tracker app, college navigation app, website for finding roommates, and Chrome extensions that enhance our academic portal for the entire college. I have been involved in various aspects of these projects, including documentation, code contribution, and UI design, poster creation, and promotional video creation.

My skills and abilities have been recognized through various achievements, including winning a hackathon conducted by the Google Developer Society in my club. I developed an extension to aid an app that tracks social media usage for Android, which was well-received. I have also contributed to the organization of various hackathons, including Vinhack, where I assisted in developing the voting portal and reviewing designs, front-end implementation, over 50 projects.

I have also completed an internship where I created screens for an AI salon app using React Native called SVAYO. Furthermore, I have contributed to the open-source community by participating in Hacktoberfest 2022, where I collaborated with a group to develop a website that converted markdown to JSX and updated a section of the website accordingly. I made the initial commits, designing the page and implementing the input box with functions for copying, downloading, and previewing the text. Unfortunately, the project did not progress further due to unforeseen circumstances.

As a user of Linux as my primary operating system, I have become adept at creating personal automation scripts to streamline my workflow and improve my overall experience. In addition, I have taken a keen interest in personalizing and theming the GNOME desktop environment to my liking. Moving forward, I am keen to extend my contribution to the open source community by contributing to my favorite applications, including but not limited to GNOME, iCloud client, Pulseaudio, and Orange SoundCloud client, among others.

My interest in custom Android ROMs has led me to explore and experiment with various ROMs, including Calyx OS and PixelExperience. I have engaged in conversations with my senior who is the creator of The Styx Project, an open-source aftermarket Android operating system, and I am eager to contribute my skills and knowledge to this project.

The setup I will be using to work on this project: 
|                      	|                         	|                                                                 	|
|----------------------	|-------------------------	|-----------------------------------------------------------------	|
| OS:                  	| Fedora 37 stable, Windows 11       	|                                                                 	|
| System Specification 	| Ryzen 7 4800U, 16GB RAM 	|                                                                 	|
| Mobile Devices       	| Android                 	| Realme XT[RMX1921] (Android 13), Wayland Emulator (Android 9.0) 	|
|                      	| iOS                     	| iPhone 12 Pro (iOS 16.3)                                        	|
| Version Control      	| Git                     	|                                                                 	|
| Coding Environment   	| Visual Studio Code      	|                                                                 	|

# Project Description
* Name: Geo Links Unshortener
* Mentors: [@biodranik](https://github.com/biodranik/), [@rtsisyk](https://github.com/rtsisyk)
### Goal: 
* **Interoperability**- The goal of this project is to develop a web service that can efficiently "unshorten" location URIs from proprietary formats into the geo: format, which is supported by almost all mapping applications. The web service should be designed to support various popular mapping providers such as Google Maps, OpenStreetMap, Baidu Map, QQ Map, Nokia HERE, Yandex Maps, 2GIS, and others. By doing so, this project aims to improve the compatibility and interoperability between different mapping applications, making it easier for users to open and use short links in Organic Maps and other mapping applications that support the geo: format.

# Objectives Overview: 

1. Go through API documentation and get familiar with link formats for all the above mentioned shortened URI's. Some things I will be looking to do in this step wouyld be looking to extract location information in supported format for OM. As confirmed by [BioDranik](https://t.me/BioDranik) on the Organic Maps telegram channel, Everything must be done on the device using pattern matching methods.
I have gone through the [Organic Maps API and Deep Links Test Page](https://omaps.app/api) to check out all the links currently working on OM app.

2. Identify sites where it is not possible to extract location information in the supported format for Organic Maps. Find potential methods to extract location information using regex and other string manipulation or pattern matching techniques.
![obtaining geo link using regex](https://i.postimg.cc/76c6917B/image.png) <br>
whereas some links like: https://osm.org/go/y4Q0IgvID
![redirect checker](https://i.postimg.cc/NFHPmYnZ/image.png)
have redirects in them which currently does not make it possible to parse the required information from the available link alone.

3. Develop code to convert the extracted location information into the geo: format. This could entail manipulating the latitude and longitude values and concatenating them into a geo: URL.
some straightforward some not
```javascript
const express = require('express');
const bodyParser = require('body-parser');
const urlParser = require('url');
const axios = require('axios');

const app = express();
app.use(bodyParser.json());

app.post('/unshorten', (req, res) => {
  const url = req.body.url;
  const urlObj = urlParser.parse(url, true);
  const hostname = urlObj.hostname;
  let geoUrl;

  switch (hostname) {
    case 'goo.gl':
      geoUrl = unshortenGooGl(url);
      break;
    case 'bit.ly':
      geoUrl = unshortenBitly(url);
      break;
    default:
      res.status(400).send({ error: 'Invalid URL' });
      return;
  }
  res.send({ geoUrl });
});


async function unshortenGooGl(url) {    //implementation to get the geoUrl from goo.gl
  try {
    const response = await axios.get(url, {
      maxRedirects: 0,
      validateStatus: function (status) {
        return status >= 200 && status < 303;
      },
    });

    const location = response.headers.location;
    const regexMatch = location.match(/@(\d+\.\d+),(\d+\.\d+)/);
    const latitude = regexMatch[1];
    const longitude = regexMatch[2];
    const geoUrl = `geo:${latitude},${longitude}`;

    return geoUrl;
  } catch (error) {
    console.error(error);
  }
}

function unshortenOpenStreetMaps(url) {
  // implementation for unshortening OpenStreetMaps short URLs
}
//similarly implementation of other map providers can be done

app.listen(3000, () => {
  console.log('Server is listening on port 3000');
});

unshortenGooGl('https://goo.gl/maps/sXnse1erRvkA6aV99')
  .then((geoUrl) => {
    console.log(geoUrl);
  })
  .catch((err) => {
    console.error(err);
  });
```

* By implementing this code, we can easily convert extracted location information into a standardized format that can be used by different mapping applications and services. This helps address the third step of developing code to convert the extracted location information into the geo: format, as it provides a straightforward solution for converting latitude and longitude values into a geo: URL format.
![output](https://i.postimg.cc/7ZWK3JfF/image.png)

4. Implement the web service as a REST API that receives short location URIs as input and returns the corresponding geo: URL. We will need to handle requests from multiple sources, parse the short link, identify the provider, and extract the location information to convert it to the geo: format. Preferably, use methods that protect location privacy.
* While searching for ideas on how to implement my server, I came across the linkify library, which caught my attention. This library is designed to detect and convert URLs in text into clickable hyperlinks. It works by searching for URLs in a given text and returning an array of objects containing information about the detected URLs, such as the URL itself, the start and end position in the text, and the type of URL (e.g., HTTP, HTTPS, FTP, etc.).
* One of the features that caught my attention is the ability to customize the output format, which could be useful for my use case. For example, I can configure the library to convert the detected URLs into geo: links if they match the URL pattern of a map provider, such as  Baidu Map, QQ Map, Nokia HERE, etc. This would simplify the code and make it more modular, as I would not need to hardcode the URL patterns for each map provider.
* Another advantage of using linkify is that it supports a wide range of URL patterns, including those with special characters and international characters. This would ensure that the library can detect and convert URLs from various sources, even if they are in non-standard formats.
* To protect a users privacy, HTTP requests can be sent to a proxy server set up by OM.
* HTTP requests from OM can be routed to services which handle scaling like AWS lambda, Google cloud functions or Cloudflares services and then make requests to map providers API to decode the coordinates or search query.

5. Write comprehensive tests to ensure that the web service functions as intended. This would involve testing sample short links from various providers to ensure their correct parsing and conversion to the geo: format. If we are finalized with this method of implementation, we can begin with the test period.
* Unit tests: To test if each function produces exoected output from given input.
* Integration tests: tests to check handling of short urls from map providers.
* End to end tests: test for bugs with respect to user interaction from when the user inputs the link to the returning of location on the app.

6. Once the web service is functional and has been tested, the process of integrating it into Organic Maps by adding new functionality to "unshorten" short links can begin. One idea is to send the URI to our web service as soon as the user enters it into the URL bar, or send the url to the server. Or as mentor @biodranik mentioned, 
* On Android, the user can click on the URL in another app, and he will be offered to open Organic Maps with this link.
* On iOS and Android, users can copy links to the keyboard and open OM, and then either paste the URL into the search box, or (even better), OM can offer to open this link automatically after detecting it in the clipboard.
* When the user clicks on the map link, it should trigger a GET request to the API endpoint with the short URI as a parameter. The API should then process the request, parse the short link, identify the provider, and extract the location information to convert it to the geo: format. Finally, the API should return the geo: URL to the client, which can then be opened with Organic Maps using the appropriate intent or URL scheme.
![scheme](https://i.postimg.cc/bwrCtpKp/Group-3.png)

# My observation
* I have found Organic Maps to be an exceptional navigation app that is both fast and accurate. I'm sure people value the privacy features of this app very much, such as no tracking and data collection. As someone who frequently navigates around my college campus, I have found that the app works seamlessly and is incredibly helpful for offline navigation. While I have come across some minor UI glitches, I have noticed that these issues have already been acknowledged in the app's Issues section, and I am confident that the team is working on resolving them. Additionally, I appreciate the app's accessibility features, which have the potential to greatly benefit users in a variety of situations. All other issues I've encountered have also been mentioned and has been asigned. I admire the dedication of the hardworking developers who treat each issue and feedback about their app with great care and I am incredibly enthusiastic about this project and believe it will provide a fantastic opportunity for personal and professional growth. Not only will I gain valuable coding experience, but I will also have the chance to hone my communication skills, collaborate with a team, shoulder significant responsibilities, and contribute to a widely-used application with hundreds of thousands of users.

* I understand that my task is to work on the compatability of opening links from popular map providers such as Google Maps, OpenStreetMap, Baidu Map, QQ Map, Nokia HERE, Yandex Maps, 2GIS as mentioned in the problem statement.

* I have gone through all the referenced link for the project and have taken it into consideration while coming up with the approach.

* Specifics about the project is discussed in [Objectives Overview](#Objectives-Overview:)

* I am excited to work with a team of experienced developers, and I am eager to learn from your expertise. I am committed to meeting the project's objectives and deadlines, and I am ready to put in the time and effort required to deliver quality work.

# Timeline
* The project has an estimate time duration of 175 hours. This schedule is a preliminary outline. To allocate the weekly tasks, I have aimed to be flexible. Typically, the final one or two days of each week will be devoted to reviewing and documenting the code or for catching up on any unfinished work. While I plan to remain in communication with my mentor regularly and receive guidance on the right track, I have also incorporated a practical feedback mechanism into the schedule to ensure that my mentor reviews a manageable amount of code at appropriate intervals.

### Week 1: Planning and Setup (10 hours)

* Research and finalize project requirements
* Create a detailed project plan and timeline
* Set up project management tools and environments
### Week 2: Initial Implementation of Server (15 hours)

* Set up server environment
* Create server endpoints for receiving and processing URLs
* Implement database schema and connect to the database
### Week 3-4: Implementing URL Shortener Support (20 hours)

* Research supported URL shorteners and their formats
* Create functions for each supported URL shortener to extract location information
* Test and verify functionality for each supported URL shortener
### Week 5-6: Implementing Geo Conversion Functionality (20 hours)

* Develop code to convert the extracted location information into the geo: format
* Test and verify functionality of the conversion function
Week 7-8: Implementing Proxy Server Functionality (25 hours)

* Develop code to send HTTP requests to supported map providers through a proxy server
* Test and verify functionality of the proxy server
### Week 9-10: Integration with  Frontend (25 hours)

* Integrate the services to app frontend:
* Connect frontend with the server endpoints
* Test and verify frontend functionality
### Week 11-12: Testing and Debugging (20 hours)

* Test the entire application and identify any bugs or issues
* Debug any issues and ensure full functionality of the app
### Week 13-14: Documentation and Deployment (20 hours)

* Write documentation for the server, writing useful comments for functions in code, etc.
* Prepare the app for deployment on a live server
* Deploy the app and test its functionality in a live environment
### Week 15: Final Testing and Review (10 hours)

* Conduct final testing to ensure all features are working properly
* Review the code and make any necessary improvements or optimizations
* Prepare for project handoff and closeout

Thank you for considering my application, and I look forward to the opportunity to contribute to the project's success.


